<?php

namespace App\Core;

class Application
{
    /** @var null The controller */
    private $url_controller = null;

    /** @var null The method (of the above controller), often also named "action" */
    private $url_action = null;

    /** @var array URL parameters */
    private $url_params = array();

    public function __construct()
    {
        // create array with URL parts in $url
        $this->splitUrl();

        // check for controller: no controller given ? then load start-page
        if (!$this->url_controller) {

            $page = new \App\Controller\LoginController();
            $page->index();

        } elseif (file_exists(APP . 'Controller/' . ucfirst($this->url_controller) . 'Controller.php')) {
            // kiểm tra xem có tồn tại Controller
            // here we did check for controller: does such a controller exist ?

            // if so, then load this file and create this controller
            // like \App\Controller\CarController
            $controller = "\\App\\Controller\\" . ucfirst($this->url_controller) . 'Controller';
            $this->url_controller = new $controller();

            // kiểm tra xem có tồn tại action trong controller
            if (method_exists($this->url_controller, $this->url_action)) {

                if (!empty($this->url_params)) {

                    // nếu có params thì sẽ gọi method có params
                    call_user_func_array(array($this->url_controller, $this->url_action), $this->url_params);
                } else {

                    // If no parameters are given, just call the method without parameters, like $this->home->method();
                    // nếu không có papams thì sẽ gọi method không có params
                    $this->url_controller->{$this->url_action}();
                }
            } else {

                if (strlen($this->url_action) == 0) {
                    // no action defined: call the default index() method of a selected controller
                    // nếu method = null thì sẽ mặc định gọi đến method index()
                    $this->url_controller->index();
                } else {
                    // nếu method khác null mà không có trong controller thì sẽ gọi đến trang thông báo lỗi
                    header('location: ' . URL . 'error');
                }
            }
        }
    }

    public function splitUrl()
    {
        if (isset($_GET['url'])) {

            // split URL
            $url = trim($_GET['url'], '/'); // cắt dấu / ở 2 đầu url
            $url = filter_var($url, FILTER_SANITIZE_URL); // kiểm tra có phải định dạng url ko trả về url nếu đúng, sai trả về false
            $url = explode('/', $url); // cắt url thành mảng phân cách nhau bằng dấu /

            $this->url_controller = isset($url[0]) ? $url[0] : null;
            $this->url_action = isset($url[1]) ? $url[1] : null;

            // Remove controller and action from the split URL
            unset($url[0], $url[1]); // xóa controller và action từ URL

            // Rebase array keys and store the URL params
            $this->url_params = array_values($url);

            // for debugging. uncomment this if you have problems with the URL
            //echo 'Controller: ' . $this->url_controller . '<br>';
            //echo 'Action: ' . $this->url_action . '<br>';
            //echo 'Parameters: ' . print_r($this->url_params, true) . '<br>';
        }
    }
}