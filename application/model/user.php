<?php

namespace App\Model;

use App\Core\Controller;

class User extends Controller
{
    public function register()
    {

    }

    public function login($username, $password)
    {
        $sql = "SELECT * FROM users WHERE username = :username AND password = :password";
        try {
            $query = $this->db->prepare($sql);
            $parameters = array(
                ":username" => $username,
                ":password" => $password
            );
            $query->execute($parameters);
            return $query->fetch();
        } catch (PDOException $e) {
            echo ($e->getMessage());
        }
    }
}